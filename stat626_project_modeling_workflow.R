# author = santiago rodriguez
# created = 2021.06.07
# purpose = Modeling work flow for stat626 class project
# methods = 
# sources:

###################################################
###################################################
###################################################
###################################################
# IMPORT LIBRARIES

# R: list of libraries
libraries = c("tidyverse","tsibble","fable","feasts")

# R: sequential install | load
for(lib in libraries){
  tmpCondition = !require(lib, character.only=TRUE, quietly=TRUE)
  if(tmpCondition)
  {
    # install
    install.packages(lib,repos='http://cran.us.r-project.org')
  }
  # load
  library(lib, character.only = TRUE, quietly = TRUE)
}

# HOUSE CLEANING
rm(lib, tmpCondition, libraries)
###################################################
###################################################
###################################################
###################################################

source("https://gitlab.com/hail_hydra/stat626_timeseries_project/-/raw/master/stat626_project_data_pipeline.R")

stat626_get_data("bitcoin") %>%
  slice_head(n=5)
