---
title: "STAT626 Time Series Project"
author: "Santiago Rodriguez"
output:
  html_document:
    #code_folding: hide
    self_contained: true
    theme: flatly
    highlight: kate
    df_print: kable
    keep_md: true
    #number_sections: true
date_created: ___
---
This was last published on:  June 09, 2021

<html>
<head>
<style>

body {
  background-color: White;
  font-family: Helvetica, Arial,sans-serif;
}

#header {
  overflow: hidden;
  background-color: #500000;
  color: white;
  display:flex;
  align-items: center;
  justify-content: center;
  position: relative;
}

h1.title {
  margin: auto;
  text-align: left;
  font-style: normal;
  font-weight: bold;
  font-size: 40px
}

hr { 
    display: block;
    margin-before: 0.5em;
    margin-after: 0.5em;
    margin-start: auto;
    margin-end: auto;
    overflow: hidden;
    border-style: inset;
    border-width: 2px;
    color:black;
}

</style>
</head>
</html>





# Team

Members, listed in alphabetical order:

- Carina Morgan
- Gaurav Munjal
- Santiago Rodriguez
- Steph Chen
- Vick Lopez

# The Data

The primary time series data used in this project will be average USD market price across major Bitcoin exchanges. As a backup, should it be needed for any reason, there is also hotel bookings data.


```r
source("https://gitlab.com/hail_hydra/stat626_timeseries_project/-/raw/master/stat626_project_data_pipeline.R")

data = stat626_get_data("bitcoin")
```

# Visuals


```r
data %>%
  ggplot(aes(x = Date, y = Value))+
  geom_line(color = "darkorange")+
  scale_y_continuous(labels = scales::dollar)+
  ggtitle("Average Bitcoin USD Market Price")+
  ylab("")+
  xlab("Date: Daily")
```

![](stat626_project_presentation_files/figure-html/btc-1.png)<!-- -->


```r
data %>%
  mutate(Value = log(Value)) %>%
  ggplot(aes(x = Date, y = Value))+
  geom_line(color = "darkorange")+
  #scale_y_continuous(labels = NULL, breaks = NULL)+
  ggtitle("Average Bitcoin USD Market Price: Log")+
  ylab("Log")+
  xlab("Date: Daily")+
  geom_smooth(method="lm",color="navy")
```

![](stat626_project_presentation_files/figure-html/btcLog-1.png)<!-- -->


```r
par(mfrow=c(2,1), mar=c(3,3,1.5,1))

plot(x = data[["Date"]],
     y = c(NA,diff(log(data$Value))),
     type = "h",
     main = "Log Diff Avgerage Bitcoin",
     ylab = "",
     xlab = "Date")

plot(x = data[["Date"]],
     y = c(NA,diff(data[["Value"]]))/lag(data[["Value"]]),
     col = "darkred",
     type = "h",
     pch = 3,
     ylab = "")
```

![](stat626_project_presentation_files/figure-html/btcLogDiff-1.png)<!-- -->




```r
acf(data[["Value"]], main = "Auto Correlation Function")
```

![](stat626_project_presentation_files/figure-html/btcACF-1.png)<!-- -->



```r
n_MA = c(30,50,100, 200)

par(mfrow=c(2,2))

for(ma in n_MA){
  v = stats::filter(data[["Value"]],
                    method = "convolution",
                    sides = 1,
                    filter = rep(1/ma, ma))
  
  astsa::tsplot(x = data[["Date"]],
                y = v,
                main = paste("Bitcoin",ma,"Day Moving Average"),
                ylab = "")  
}
```

![](stat626_project_presentation_files/figure-html/btcMA-1.png)<!-- -->


